// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. 
// Технические требования:
// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

let firstName = prompt("Please enter your first name");
while (!firstName) {
    firstName = prompt("Error. Please enter your first name again");
}

let lastName = prompt("Please enter your last name");
while (!lastName) {
    lastName = prompt("Error. Please enter your last name again");
}

let birthday = prompt("Please enter your date of birth like this dd.mm.yyyy");
let birthdayArr = birthday.split('.');
while (!Number.isInteger(+birthdayArr[0]) || !Number.isInteger(+birthdayArr[1]) || !Number.isInteger(+birthdayArr[2])) {
    birthday = prompt("Error. Please enter your date of birth again");
    birthdayArr = birthday.split('.');
}

birthday = birthdayArr[2] + '-' + birthdayArr[1] + '-' + birthdayArr[0];

function createNewUser(userName, userSurname, birthday) {
    let newUser = {
        userFirstName: userName,
        userLastName: userSurname,
        userBirthDate: birthday,
        getUserLogin: function() {
            return this.userFirstName[0].toLowerCase() + this.userLastName.toLowerCase();
        },
        setFirstName: function(newFirstName) {
            Object.defineProperty(this, "userFirstName", { writable: true, configurable: true });
            this.userFirstName = newFirstName;
            Object.defineProperty(this, "userFirstName", { writable: false, configurable: true });
        },
        setLastName: function(newLastName) {
            Object.defineProperty(this, "userLastName", { writable: true, configurable: true });
            this.userLastName = newLastName;
            Object.defineProperty(this, "userLastName", { writable: false, configurable: true });
        },
        getAge: function() {
            return Math.floor((new Date().getTime() - new Date(this.userBirthDate)) / (24 * 3600 * 365.25 * 1000));
        },
        getUserPassword: function() {
            let birthdayArr = this.userBirthDate.split('-');
            return this.userFirstName[0].toUpperCase() + this.userLastName.toLowerCase() + birthdayArr[0];
        }
    }

    Object.defineProperty(newUser, "userFirstName", { writable: false, configurable: true });
    Object.defineProperty(newUser, "userLastName", { writable: false, configurable: true });

    return newUser;
}

const user = createNewUser(firstName, lastName, birthday);

console.log(user.getUserLogin());
console.log(user.getAge());
console.log(user.getUserPassword());