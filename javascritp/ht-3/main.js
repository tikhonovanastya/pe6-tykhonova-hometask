"use strict";

//Реализовать функцию, которая будет производить математические операции с введеными пользователем числами. 
// Технические требования:
// Считать с помощью модального окна браузера два числа.
// Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
// Создать функцию, в которую передать два значения и операцию.
// Вывести в консоль результат выполнения функции.
// Необязательное задание продвинутой сложности:
// После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).

let firstNumber = +prompt("Please enter the first number");

while (!Number.isInteger(firstNumber)) {
    firstNumber = +prompt("Error. Please enter the first number again");
}

let secondNumber = +prompt("Please enter the second number");

while (!Number.isInteger(secondNumber)) {
    secondNumber = +prompt("Error. Please enter the second number again");
}

let mathSign = prompt("Please enter the one of the mathematical operations: +, -, * or /");

while (!(mathSign == "+" || mathSign == "-" || mathSign == "*" || mathSign == "/")) {
    mathSign = prompt("Error. Please enter the one of the mathematical operations: +, -, * or /");
}

function mathOperation(num1, num2, sign) {
    if (sign == "+") {
        return num1 + num2;
    } else if (sign == "-") {
        return num1 - num2;
    } else if (sign == "*") {
        return num1 * num2;
    } else {
        return num1 / num2;
    }
}

console.log(mathOperation(firstNumber, secondNumber, mathSign));