"use strict";

/* Написать реализацию кнопки "Показать пароль". 

Технические требования:
В файле index.html лежит разметка для двух полей ввода пароля.
По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения

После нажатия на кнопку страница не должна перезагружаться
Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее. */

document.onclick = function(event) {
    if (event.target.classList.contains("icon-password")) {
        if (event.target.classList.contains("fa-eye")) {
            event.target.classList.remove("fa-eye");
            event.target.classList.add("fa-eye-slash");
            let myId = event.target.getAttribute("data-type");
            console.log(myId);
            document.getElementById(myId).setAttribute("type", "text");
        } else {
            event.target.classList.remove("fa-eye-slash");
            event.target.classList.add("fa-eye");
            let myId = event.target.getAttribute("data-type");
            document.getElementById(myId).setAttribute("type", "password");
        }
    } else if (event.target.classList.contains("btn")) {
        if (document.getElementById("pass1").value == document.getElementById("pass2").value) {
            document.getElementById("error").textContent = "";
            alert("You are welcome");
        } else {
            document.getElementById("error").textContent = "Нужно ввести одинаковые значения";
        }
    }
}