"use strict";

/*Добавить в домашнее задание HTML/CSS №4 (Flex/Grid) различные эффекты с использованием jQuery

Технические требования:

Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы.
При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.
Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться кнопка "Наверх" с фиксированным позиционариванием. При клике на нее - плавно прокрутить страницу в самый верх.
Добавить под одной из секций кнопку, которая будет выполнять функцию slideToggle() (прятать и показывать по клику) для данной секции. */

$(document).ready(function(event) {


    $(document).on("click", ".menu_link", function(event) {
        let menuItem = $(this).attr("href");
        if (menuItem[0] == "#") {
            event.preventDefault();
            let top = $(menuItem).offset().top;
            $("body, html").animate({ scrollTop: top }, 1000)
        }
    })

    $(window).scroll(function() {
        if ($(this).scrollTop() > (window.innerHeight - 170)) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });

    $("#toTop").click(function() {
        $("html, body").animate({ scrollTop: 0 }, 1100);
    });

    $(document).on("click", "#toggleButton", function() {
        if ($(".news").is(":visible")) {
            $("#toggleButton").text("Показать секцию");
        } else {
            $("#toggleButton").text("Скрыть секцию");
        }
        $(".news").slideToggle("slow");
    });
})