"use strict";

/* Реализовать программу, показывающую циклично разные картинки. 

Технические требования:

В папке banners лежит HTML код и папка с картинками.
При запуске программы на экране должна отображаться первая картинка.
Через 3 секунды вместо нее должна быть показана вторая картинка.
Еще через 3 секунды - третья.
Еще через 3 секунды - четвертая.
После того, как покажутся все картинки - этот цикл должен начаться заново.
При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.
По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки.
Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл продолжается с той картинки, которая в данный момент показана на экране.
Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.

Необязательное задание продвинутой сложности:
При запуске программы на экране должен быть таймер с секундами и миллисекундами, показывающий сколько осталось до показа следующей картинки. Делать скрытие картинки и показывание новой картинки постепенным (анимация fadeOut / fadeIn) в течение 0.5 секунды. */

let imagesArr = document.querySelectorAll(".image-to-show");
let imageIndex = 0;
let isPaused = false;
let counter = document.querySelector(".counter");
let countDownNumber = 3;
let play = document.querySelector(".play");
let pause = document.querySelector(".pause");


function mySlider() {
    if (isPaused) { return };
    for (let i = 0; i < imagesArr.length; i++) {
        imagesArr[i].classList.remove("active");
    }
    imagesArr[imageIndex].classList.add("active");
    countDownNumber = 3;
    imageIndex++;
    if (imageIndex == imagesArr.length) {
        imageIndex = 0;
    }
}

play.onclick = function(event) {
    isPaused = false;
    play.style.display = "none";
    pause.style.display = "block";
}

pause.onclick = function(event) {
    isPaused = true;
    pause.style.display = "none";
    play.style.display = "block";
}

function countDown() {
    if (countDownNumber <= 0) {
        return;
    }
    counter.innerHTML = countDownNumber + " sec";
    countDownNumber--;
}

mySlider();
setInterval(mySlider, 3000);
setInterval(countDown, 1000);