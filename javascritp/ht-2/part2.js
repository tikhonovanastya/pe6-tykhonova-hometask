//Считать два числа, m и n. Вывести в консоль все простые числа (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE) в диапазоне от m до n (меньшее из введенных чисел будет m, бОльшее будет n). Если хотя бы одно из чисел не соблюдает условие валидации, указанное выше, вывести сообщение об ошибке, и спросить оба числа заново.

let n;
let m;

let firstNumber = +prompt("Please enter the first number");

while (!Number.isInteger(firstNumber)) {
    firstNumber = +prompt("Error. Please enter the first number again");
}

let secondNumber = +prompt("Please enter the second number");

while (!Number.isInteger(secondNumber)) {
    secondNumber = +prompt("Error. Please enter the second number again");
}

m = Math.min(firstNumber, secondNumber);
n = Math.max(firstNumber, secondNumber);

let breacker = false;

for (let i = m; i <= n; i++) {
    for (let j = 2; j < i; j++) {
        if (i % j == 0) {
            breacker = true;
            break;
        }
    }

    if (breacker) {
        breacker = false;
        continue;
    }

    if (i > 1) {
        console.log(i);
    }
}