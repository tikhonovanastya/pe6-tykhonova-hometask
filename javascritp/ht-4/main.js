// Реализовать функцию для создания объекта "пользователь". 
// Технические требования:
// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
// При вызове функция должна спросить у вызывающего имя и фамилию.
// Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
// Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.
// Необязательное задание продвинутой сложности:
// Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.

let firstName = prompt("Please enter your first name");
while (!firstName) {
    firstName = prompt("Error. Please enter your first name again");
}

let lastName = prompt("Please enter your last name");
while (!lastName) {
    lastName = prompt("Error. Please enter your last name again");
}

function createNewUser(userName, userSurname) {
    let newUser = {
        userFirstName: userName,
        userLastName: userSurname,
        getUserLogin: function() {
            return this.userFirstName[0].toLowerCase() + this.userLastName.toLowerCase();
        },
        setFirstName: function(newFirstName) {
            Object.defineProperty(this, "userFirstName", { writable: true, configurable: true });
            this.userFirstName = newFirstName;
            Object.defineProperty(this, "userFirstName", { writable: false, configurable: true });
        },
        setLastName: function(newLastName) {
            Object.defineProperty(this, "userLastName", { writable: true, configurable: true });
            this.userLastName = newLastName;
            Object.defineProperty(this, "userLastName", { writable: false, configurable: true });
        }
    }

    Object.defineProperty(newUser, "userFirstName", { writable: false, configurable: true });
    Object.defineProperty(newUser, "userLastName", { writable: false, configurable: true });

    return newUser;
}

const user = createNewUser(firstName, lastName);

console.log(user.getUserLogin());

user.userFirstName = 'Marina';
user.userLastName = 'Petrova';

console.log(user.getUserLogin());

user.setFirstName('Lili');
user.setLastName('Ivanova');

console.log(user.getUserLogin());