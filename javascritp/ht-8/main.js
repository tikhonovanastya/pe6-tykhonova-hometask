"use strict";

//Создать поле для ввода цены с валидацией.
// Технические требования:
// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
// В папке img лежат примеры реализации поля ввода и создающегося span.

let priceInput = document.querySelector(".priceInput input");

priceInput.onfocus = function() {
    this.style.border = "2px solid green";
};

priceInput.onblur = function() {
    this.style.border = "1px solid black";
    this.style.borderRadius = "3px";
    if (this.value < 0 || !Number.isInteger(+this.value)) {
        priceInput.style.border = "2px solid red";
        document.getElementsByClassName("error")[0].innerHTML = "Please enter correct price";
        priceInput.value = "";
    } else {
        let newSpan = document.createElement("span");
        newSpan.innerHTML = `Текущая цена: ${this.value}`;
        document.getElementsByClassName("spanHolder")[0].innerHTML = newSpan.innerHTML;
        priceInput.style.color = "green";
    }
};

priceInput.oninput = function() {
    button.style.display = "inline";
}

let button = document.querySelector(".closer");

button.onclick = function() {
    priceInput.value = "";
    // this.remove(); OR
    this.style.display = "none";
    document.getElementsByClassName("spanHolder")[0].innerHTML = `Текущая цена: `;
};