"use strict";

/* Реализовать функцию подсветки нажимаемых клавиш. 

Технические требования:

В файле index.html лежит разметка для кнопок.
Каждая кнопка содержит в себе название клавиши на клавиатуре
По нажатию указанных клавиш - та кнопка, на которой написана эта буква, должна окрашиваться в синий цвет. При этом, если какая-то другая буква уже ранее была окрашена в синий цвет - она становится черной. Например по нажатию Enter первая кнопка окрашивается в синий цвет. Далее, пользователь нажимает S, и кнопка S окрашивается в синий цвет, а кнопка Enter опять становится черной. */

document.onkeyup = function(event) {
    let letterAttribute = document.querySelector(`[data-key = '${event.code}']`);

    if (letterAttribute) {
        document.querySelectorAll(".button-color").forEach(element => {
            element.classList.remove("button-color");
        });

        letterAttribute.classList.add("button-color");
    }
}