"use strict";

/* Реализовать возможность смены цветовой темы сайта пользователем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

Взять любое готовое домашнее задание по HTML/CSS.
Добавить на макете кнопку "Сменить тему".
При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
Выбранная тема должна сохраняться и после перезагрузки страницы */

let newThemeButton = document.querySelector(".themeChanger");


newThemeButton.onclick = function(event) {
    if (document.body.classList.contains("page_body")) {
        document.body.classList.remove("page_body");
        localStorage.setItem("hasNewTheme", 0);
    } else {
        document.body.classList.add("page_body");
        localStorage.setItem("hasNewTheme", 1);
    }
}

if (+localStorage.getItem("hasNewTheme")) {
    document.body.classList.add("page_body");
}